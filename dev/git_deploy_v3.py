from sshtunnel import SSHTunnelForwarder
import git
import os
from pathlib import Path
import config
import db_execute_files
from filePath import filePathExtension
import configInit

BITBUCKET_DEPLOYMENT_ENVIRONMENT = os.environ.get("BITBUCKET_DEPLOYMENT_ENVIRONMENT")
BITBUCKET_BRANCH = os.environ.get("BITBUCKET_BRANCH")

print(f"BITBUCKET_DEPLOYMENT_ENVIRONMENT - ", {BITBUCKET_DEPLOYMENT_ENVIRONMENT})
print(f"\nBITBUCKET_BRANCH -", {BITBUCKET_BRANCH})

print("\nos.environ - ", os.environ)

repo = git.Repo(".")
print("\ncat_file_header - ", repo.git.cat_file_header)

diff = repo.git.diff("HEAD~1", name_only=True)
difff = repo.git.diff(name_only=True)

print("\nrepo.git.diff('HEAD~1', name_only=True)", diff)

print("\nrepo.git.diff(name_only=True)", difff)

def processData():
    dbConfigObj = config.Config()
    dbConfigObj = configInit.configInit("master", dbConfigObj)
    try:
        with SSHTunnelForwarder(
            (dbConfigObj.get_ec2_endpoint()),
            ssh_username=dbConfigObj.get_ec2_username(),
            ssh_pkey=dbConfigObj.get_ssh_pem_location(),
            remote_bind_address=(
                dbConfigObj.get_coms8_host(),
                dbConfigObj.get_coms8_port(),
            ),
        ) as tunnel:

            print("***SSH Tunnel Established***")
            db = configInit.getDbConn(dbConfigObj.get_coms8_password(), tunnel.local_bind_port)

            # Run sample query in the database to validate connection
            for file in diff.split("\n"):

                if file.endswith(filePathExtension.SQLFILEEXTENSION.value):

                    open_file = open(file, "r")
                    read_file = open_file.read()

                    folder_server = file.split("/")[0]

                    if folder_server != "templates":

                        print(
                            """\n\n\n
                        *******************************************************************************
                        *                   NEWLY COMMITTED SQL FILES STARTED EXECUTING !!!           *
                        *******************************************************************************
                            """
                        )

                        folder_schema = file.split("/")[1]
                        folder_object_type = file.split("/")[2]
                        folder_object_name = file.split("/")[3].replace(".sql", "")

                        db_execute_files.execute_files(
                            db,
                            read_file,
                            folder_object_name,
                            folder_object_type,
                            folder_schema,
                            dbConfigObj.get_coms8_host(),
                            folder_object_name,
                            folder_server,
                        )
                        db.commit()

                elif file.endswith(filePathExtension.PYTHONFILEEXTENSION.value):
                    print(""" *  NEWLY COMMITTED PYTHON FILES * """)
                    print(file + "\n")

                else:
                    print(""" *     OTHER FILES *   """)
                    print(file + "\n")

    except Exception as e:
        print("Exception Occured", e)

    finally:
        db.close()

processData()