import configparser
import pymysql


def configInit(fromBranch, dbConfigObj):
    dbConfig = configparser.ConfigParser()
    dbConfig.read("config/mysql/coms8.ini")
    dbConfigObj.set_coms8_host(dbConfig.get(fromBranch, "host"))
    dbConfigObj.set_coms8_user(dbConfig.get(fromBranch, "user"))
    dbConfigObj.set_coms8_password(dbConfig.get(fromBranch, "password"))
    dbConfigObj.set_coms8_initial_database(dbConfig.get(fromBranch, "initial_database"))
    dbConfigObj.set_ec2_endpoint(dbConfig.get("ec2", "ec2_endpoint"))
    dbConfigObj.set_ec2_username(dbConfig.get("ec2", "ec2_username"))
    dbConfigObj.set_ssh_pem_location(dbConfig.get("ec2", "ssh_pem_location"))
    return dbConfigObj

def getDbConn(coms8_password, localBindPort):
    return pymysql.connect(
        host="127.0.0.1",
        user="admin",
        password=coms8_password,
        port=localBindPort,
    )