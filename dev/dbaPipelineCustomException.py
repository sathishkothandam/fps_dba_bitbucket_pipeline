class CommentValueException(Exception):
    def __init__(self, comment, message = "Comment should be more than 20 char!!"):
        self.comment = comment
        self.message = message
        super().__init__(self.message)

class FolderObjectNameException(Exception):
    def __init__(self, folderObjectName, messgae = "Folder Object Name Should be same as table name"):
        self.folderObjectName = folderObjectName
        self.message = messgae
        super().__init__(self.message)

class EmailIdValidation(Exception):
    def __init__(self, email,message = "Please enter a valid emailId - For Example : xyz@xyz.com"):
        self.email = email
        self.message = message
        super().__init__(message)