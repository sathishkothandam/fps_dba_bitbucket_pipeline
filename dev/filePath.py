from enum import Enum

class filePathExtension(Enum):
    SQLFILEEXTENSION = '.sql'
    PYTHONFILEEXTENSION = '.py'