from sshtunnel import SSHTunnelForwarder
import pymysql
import git
import os
from pathlib import Path
import sys
import configparser
import subprocess
#from pandas import DataFrame

def execute_files(db, readfile, objecttype):

    print("!!! Executing " + objecttype + " : for " + folder_object_name +  " @ MySQL server " + folder_server )
    try:
        with db.cursor(pymysql.cursors.DictCursor) as cur:

            meta_data_insert_query_start = read_file.find("INSERT INTO dba.db_objects_master_data")
            meta_data_insert_query_end = read_file.find("comments = values(comments);")+28

            if meta_data_insert_query_end <= 28:
                print("META DATA not in FORMAT!! string not found 'comments = values(comments);'") 
                exit
            
            meta_data_insert_query = read_file[meta_data_insert_query_start:meta_data_insert_query_end]

            print('\n'+ meta_data_insert_query +'\n')
            
            cur.execute(meta_data_insert_query)

            print('\n !!! META DATA ADDED into DB !!! \n') # insert meta data 

            object_ddl_start = meta_data_insert_query_end + 1 
            object_ddl_end   = len(read_file)
            object_ddl = read_file[object_ddl_start:object_ddl_end]

            object_drop_ddl_start = object_ddl.find("DROP "+ objecttype +" IF EXISTS")
            object_drop_ddl_end = object_ddl.find(";")+1
            object_drop_ddl = object_ddl[object_drop_ddl_start:object_drop_ddl_end]

            print('\n'+ object_drop_ddl + '\n')
            DROP_OBJECT = object_drop_ddl
            cur.execute(object_drop_ddl) # drop table 

            print("!!!DROP " + objecttype + " query : " + object_drop_ddl + " : EXECUTED!!!")

            object_create_query_start = object_drop_ddl_end + 1
            object_create_query_end = len(object_ddl)
            object_create_query = object_ddl[object_create_query_start:object_create_query_end]

            print('\n'+ object_create_query + '\n')
            CREATE_OBJECT = object_create_query
            cur.execute(object_create_query) # create table

            select_query_for_insert = "SELECT * FROM dba.db_objects_master_data ORDER BY table_create_time DESC LIMIT 1;"

            cur.execute(select_query_for_insert)
            
            print()
            META_DATA = cur.fetchall() # DataFrame(cur.fetchall()).set_index('instance_name').T
            print(META_DATA)
            # META_DATA['DROP_OBJECT'] =  "test"
            # META_DATA['CREATE_OBJECT'] = "test"

            print("printing meta data")
            print(META_DATA)

            for row in META_DATA:

                instance_name	=	row['instance_name']
                business_need	=	row['business_need']
                business_owner	=	row['business_owner']
                client_name	=	row['client_name']
                comments	=	row['comments']
                decommission_date	=	row['decommission_date']
                inception_date	=	row['inception_date']
                last_updated_age_in_years	=	row['last_updated_age_in_years']
                object_name	=	row['object_name']
                schema_name	=	row['schema_name']
                status	=	row['status']
                sub_type_of_object	=	row['sub_type_of_object']
                table_create_time	=	row['table_create_time']
                table_last_updated	=	row['table_last_updated']
                technical_owner	=	row['technical_owner']
                object_type	=	row['object_type']

            JSON_OUTPUT = """{"instance_name": "%s","business_need": "%s","business_owner": "%s","client_name": "%s","comments": "%s","decommission_date": "%s","inception_date": "%s","last_updated_age_in_years": "%s","object_name": "%s","schema_name": "%s","status": "%s","sub_type_of_object": "%s","table_create_time": "%s","table_last_updated": "%s","technical_owner": "%s","object_type": "%s","DROP_OBJECT": "%s","CREATE_OBJECT": "%s"}""" %(instance_name,business_need,business_owner,client_name,comments,decommission_date,inception_date,last_updated_age_in_years,object_name,schema_name,status,sub_type_of_object,table_create_time,table_last_updated,technical_owner,object_type,DROP_OBJECT,CREATE_OBJECT)

            print(JSON_OUTPUT)
            #print("".join(JSON_OUTPUT.split("\n")))
            #print(JSON_OUTPUT.replace('\n', '\\n').replace('\t', '\\t'))
            file = open('lambda-output.json','w')
            file.write(JSON_OUTPUT.replace('\n', '\\n').replace('\t', '\\t')) 
            file.close()

    finally:
        print('\n !!! ' + objecttype + ' : ' + folder_object_name + 'created in database - ' + folder_schema + ' ,server : ' + folder_server + '!!!')
        #db.close()


config = configparser.ConfigParser()
config.read('config/mysql/coms8.ini')

coms8_host = config.get('master','host') 
coms8_user = config.get('master','user') 
coms8_port = 3306
coms8_password = config.get('master','password') 
coms8_initial_database = config.get('master','initial_database') 

ec2_endpoint = config.get('ec2','ec2_endpoint') 
ec2_username = config.get('ec2','ec2_username') 
ssh_pem_location = config.get('ec2','ssh_pem_location') 

#AWS_REGION  = os.environ.get('AWS_REGION')
#AWS_SECRET_ACCESS_KEY =  os.environ.get('AWS_SECRET_ACCESS_KEY')
#AWS_SECRET_KEY_ID  = os.environ.get('AWS_SECRET_KEY_ID')

BITBUCKET_DEPLOYMENT_ENVIRONMENT =  os.environ.get('BITBUCKET_DEPLOYMENT_ENVIRONMENT')
BITBUCKET_BRANCH =  os.environ.get('BITBUCKET_BRANCH')

print("BITBUCKET_DEPLOYMENT_ENVIRONMENT")
print(BITBUCKET_DEPLOYMENT_ENVIRONMENT)

print("BITBUCKET_BRANCH")
print(BITBUCKET_BRANCH)


host  = coms8_host # os.environ.get('host')
password = coms8_password # os.environ.get('password')

print("os.environ")
print(os.environ)

# print(os.environ)

repo = git.Repo(".")
diff = repo.git.diff('HEAD~1', name_only=True)
difff = repo.git.diff(name_only=True)

print("repo.git.diff('HEAD~1', name_only=True)")
print(diff)
print("repo.git.diff(name_only=True)")
print(difff)

# Lets see if it works

with SSHTunnelForwarder(
    (ec2_endpoint),
    ssh_username=ec2_username,
    ssh_pkey=ssh_pem_location,
    remote_bind_address=(host, coms8_port)
) as tunnel:

    print("***SSH Tunnel Established***")
    db = pymysql.connect(
        host='127.0.0.1', user="admin",
        password = password, port=tunnel.local_bind_port
    )
    # Run sample query in the database to validate connection
    for file in diff.split('\n'):
        
        if file.endswith('.sql'):
            print("""\n\n\n
*******************************************************************************
*                   NEWLY COMMITTED SQL FILES STARTED EXECUTING !!!           *
*******************************************************************************
                        """)

            open_file  = open(file,'r')
            read_file  = open_file.read()
            
            mysqlshell = "mysql --host 127.0.0.1 -u %s -p%s  < %s " % (coms8_user,coms8_password,file)

            folder_server = file.split("/")[0]
            folder_schema = file.split("/")[1]
            folder_object_type = file.split("/")[2]
            folder_object_name = file.split("/")[3]

            if folder_object_type == "table":
                execute_files(db,read_file,"TABLE")

            elif folder_object_type == "view":
                execute_files(db,read_file,"VIEW")

            elif folder_object_type == "function":
                execute_files(db,read_file,"FUNCTION")

            elif folder_object_type == "procedure":
                execute_files(db,read_file,"PROCEDURE")

            else:
                print("                        file : " + file + "will NOT execute CoMS8")
             
        elif file.endswith('.py'):

            print(""" *  NEWLY COMMITTED PYTHON FILES * """)
            print(file + '\n')

        else :
            print(""" *     OTHER FILES *   """)
            print(file + '\n')

    db.close()