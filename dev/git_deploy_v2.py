from sshtunnel import SSHTunnelForwarder
import pymysql
import git
import os
from pathlib import Path
import sys
import configparser
import subprocess
#from pandas import DataFrame

def execute_files(db, read_file, object_name, object_type , schema_name , instance_name):

    print("[INFO] Creating " + object_type + f" : {schema_name}." + folder_object_name +  " @ MySQL server " + folder_server)
    try:
        with db.cursor(pymysql.cursors.DictCursor) as cur:

            meta_data_info_start = read_file.find("[user edit]")
            meta_data_info_end   = read_file.find("[/user edit]")

            if read_file.find(folder_object_name) < 0:
                print(f"[WARNING] File name is not same as object_name {folder_object_name}")

            user_edit = read_file[meta_data_info_start:meta_data_info_end]

            client_name        = user_edit.split('=')[1].replace('technical_owner','').strip()
            technical_owner    = user_edit.split('=')[2].replace('business_owner','').strip()
            business_owner     = user_edit.split('=')[3].replace('business_need','').strip()
            business_need      = user_edit.split('=')[4].replace('sub_type_of_object','').strip()
            sub_type_of_object = user_edit.split('=')[5].replace('decommission_date','').strip()
            decommission_date  = user_edit.split('=')[6].replace('comments','').strip()
            comments           = user_edit.split('=')[7].strip()
            status             = 'in-use' 

            insert_meta_data_sql = f"""
            INSERT INTO dba.meta_data
            VALUES
            (
                        '{instance_name}'
                        , '{schema_name}'
                        , '{object_name}'
                        , '{status}'
                        , '{client_name}'
                        , '{technical_owner}'
                        , '{business_owner}'
                        , '{business_need}'
                        , '{object_type}'
                        , '{sub_type_of_object}'
                        , NOW()
                        , '{comments}' 
                            )
            ON DUPLICATE KEY UPDATE
                        client_name = values(client_name),
                        technical_owner = values(technical_owner),
                        business_owner = values(business_owner),
                        business_need = values(business_need),
                        object_type = values(object_type),
                        sub_type_of_object = values(sub_type_of_object),
                        object_create_time = values(object_create_time),
                        comments = values(comments);"""



            object_ddl_start = meta_data_info_end
            object_ddl_end   = len(read_file)
            object_ddl = read_file[object_ddl_start:object_ddl_end]

            object_drop_ddl_start = object_ddl.find("DROP "+ object_type.upper() +" IF EXISTS")

            object_drop_ddl_end = object_ddl.find(";")+1
            object_drop_ddl = object_ddl[object_drop_ddl_start:object_drop_ddl_end]

            print('[INFO] Drop object DDL \n'+ object_drop_ddl + '\n')
            DROP_OBJECT = object_drop_ddl
            cur.execute(object_drop_ddl) # drop table 

            print("[INFO] Ojbect Dropped " + object_type + " query : " + object_drop_ddl)

            object_create_query_start = object_drop_ddl_end + 1
            object_create_query_end = len(object_ddl)
            object_create_query = object_ddl[object_create_query_start:object_create_query_end]

            print('[INFO] Create object DDL \n'+ object_create_query + '\n')
            CREATE_OBJECT = object_create_query
            cur.execute(object_create_query) # create table

            print('[INFO]INSERT meta data into table (ddl) \n'+ insert_meta_data_sql +'\n')
            
            cur.execute(insert_meta_data_sql)

            print(f'\n [INFO] {object_type} : {folder_object_name} created & meta data added in db- success \n') # insert meta data 
            select_query_for_insert = "SELECT * FROM dba.meta_data ORDER BY object_create_time DESC LIMIT 1;"

            cur.execute(select_query_for_insert)
            
            META_DATA = cur.fetchall() # DataFrame(cur.fetchall()).set_index('instance_name').T
            
            # print(META_DATA)
            # META_DATA['DROP_OBJECT'] =  "test"
            # META_DATA['CREATE_OBJECT'] = "test"

            print("[INFO]printing meta data")
            print(META_DATA)

            # for row in META_DATA:

            #     instance_name	=	row['instance_name']
            #     business_need	=	row['business_need']
            #     business_owner	=	row['business_owner']
            #     client_name	=	row['client_name']
            #     comments	=	row['comments']
            #     decommission_date	=	row['decommission_date']
            #     inception_date	=	row['inception_date']
            #     last_updated_age_in_years	=	row['last_updated_age_in_years']
            #     object_name	=	row['object_name']
            #     schema_name	=	row['schema_name']
            #     status	=	row['status']
            #     sub_type_of_object	=	row['sub_type_of_object']
            #     table_create_time	=	row['table_create_time']
            #     table_last_updated	=	row['table_last_updated']
            #     technical_owner	=	row['technical_owner']
            #     object_type	=	row['object_type']

            # JSON_OUTPUT = """{"instance_name": "%s","business_need": "%s","business_owner": "%s","client_name": "%s","comments": "%s","decommission_date": "%s","inception_date": "%s","last_updated_age_in_years": "%s","object_name": "%s","schema_name": "%s","status": "%s","sub_type_of_object": "%s","table_create_time": "%s","table_last_updated": "%s","technical_owner": "%s","object_type": "%s","DROP_OBJECT": "%s","CREATE_OBJECT": "%s"}""" %(instance_name,business_need,business_owner,client_name,comments,decommission_date,inception_date,last_updated_age_in_years,object_name,schema_name,status,sub_type_of_object,table_create_time,table_last_updated,technical_owner,object_type,DROP_OBJECT,CREATE_OBJECT)

            # print(JSON_OUTPUT)
            # #print("".join(JSON_OUTPUT.split("\n")))
            # #print(JSON_OUTPUT.replace('\n', '\\n').replace('\t', '\\t'))
            # file = open('PROD/lambda-results/lambda-output.json','w')
            # file.write(JSON_OUTPUT.replace('\n', '\\n').replace('\t', '\\t')) 
            # file.close()

    finally:
        print('try-catch-finally')
        # print('\n [INFO] ' + object_type + ' : ' + folder_object_name + ' created in database - ' + folder_schema + ' ,server : ' + folder_server + '!.')
        # db.close()


config = configparser.ConfigParser()
config.read('config/mysql/coms8.ini')

coms8_host = config.get('master','host') 
coms8_user = config.get('master','user') 
coms8_port = 3306
coms8_password = config.get('master','password') 
coms8_initial_database = config.get('master','initial_database') 

ec2_endpoint = config.get('ec2','ec2_endpoint') 
ec2_username = config.get('ec2','ec2_username') 
ssh_pem_location = config.get('ec2','ssh_pem_location') 

#AWS_REGION  = os.environ.get('AWS_REGION')
#AWS_SECRET_ACCESS_KEY =  os.environ.get('AWS_SECRET_ACCESS_KEY')
#AWS_SECRET_KEY_ID  = os.environ.get('AWS_SECRET_KEY_ID')
BITBUCKET_DEPLOYMENT_ENVIRONMENT =  os.environ.get('BITBUCKET_DEPLOYMENT_ENVIRONMENT')
BITBUCKET_BRANCH =  os.environ.get('BITBUCKET_BRANCH')

print(f"BITBUCKET_DEPLOYMENT_ENVIRONMENT - {BITBUCKET_DEPLOYMENT_ENVIRONMENT}")
print(f"BITBUCKET_BRANCH - {BITBUCKET_BRANCH}")

host  = coms8_host # os.environ.get('host')
password = coms8_password # os.environ.get('password')

# print(os.environ)
print("os.environ")
print(os.environ)

repo = git.Repo(".")
print("cat_file_header",repo.git.cat_file_header)

diff = repo.git.diff('HEAD~1', name_only=True)
difff = repo.git.diff(name_only=True)

print("repo.git.diff('HEAD~1', name_only=True)")
print(diff)

print("repo.git.diff(name_only=True)")
print(difff)

# Lets see if it works

with SSHTunnelForwarder(
    (ec2_endpoint),
    ssh_username=ec2_username,
    ssh_pkey=ssh_pem_location,
    remote_bind_address=(host, coms8_port)
) as tunnel:

    print("***SSH Tunnel Established***")
    db = pymysql.connect(
        host='127.0.0.1', user="admin",
        password = password, port=tunnel.local_bind_port
    )
    # Run sample query in the database to validate connection
    for file in diff.split('\n'):
        
        if file.endswith('.sql'):

            open_file  = open(file,'r')
            read_file  = open_file.read()
            
            mysqlshell = "mysql --host 127.0.0.1 -u %s -p%s  < %s " % (coms8_user,coms8_password,file)

            folder_server = file.split("/")[0]

            if folder_server != 'templates':

                print("""\n\n\n
                *******************************************************************************
                *                   NEWLY COMMITTED SQL FILES STARTED EXECUTING !!!           *
                *******************************************************************************
                        """)

                folder_schema = file.split("/")[1]
                folder_object_type = file.split("/")[2]
                folder_object_name = file.split("/")[3].replace(".sql","")

                execute_files(db, read_file, folder_object_name, folder_object_type , folder_schema , coms8_host)
             
        elif file.endswith('.py'):

            print(""" *  NEWLY COMMITTED PYTHON FILES * """)
            print(file + '\n')

        else :
            print(""" *     OTHER FILES *   """)
            print(file + '\n')

    db.close()
