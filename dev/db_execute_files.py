import pymysql
import validateData
import metaDataConfig


def execute_files(
    db,
    read_file,
    object_name,
    object_type,
    schema_name,
    instance_name,
    folder_object_name,
    folder_server,
):

    print(
        "[INFO] Creating "
        + object_type
        + f" : {schema_name}."
        + folder_object_name
        + " @ MySQL server "
        + folder_server
    )
    try:
        with db.cursor(pymysql.cursors.DictCursor) as cur:

            meta_data_info_start = read_file.find("[user edit]")
            meta_data_info_end = read_file.find("[/user edit]")

            user_edit = read_file[meta_data_info_start:meta_data_info_end]

            dbMetaDataConfig = metaDataConfig.metaDataConfig(user_edit)

            validateData.validateData(
                read_file,
                folder_object_name,
                'ABCD',
                dbMetaDataConfig.get_technical_owner(),
                dbMetaDataConfig.get_business_owner(),
            )

            insert_meta_data_sql = f"""
            INSERT INTO dba.meta_data
            VALUES
            (
                        '{instance_name}'
                        , '{schema_name}'
                        , '{object_name}'
                        , '{dbMetaDataConfig.get_status()}'
                        , '{dbMetaDataConfig._client_name}'
                        , '{','.join(map(str, dbMetaDataConfig._technical_owner))}'
                        , '{','.join(map(str, dbMetaDataConfig._business_owner))}'
                        , '{dbMetaDataConfig._business_need}'
                        , '{object_type}'
                        , '{dbMetaDataConfig._sub_type_of_object}'
                        , NOW()
                        , '{dbMetaDataConfig.get_comments()}' 
                            )
            ON DUPLICATE KEY UPDATE
                        client_name = values(client_name),
                        technical_owner = values(technical_owner),
                        business_owner = values(business_owner),
                        business_need = values(business_need),
                        object_type = values(object_type),
                        sub_type_of_object = values(sub_type_of_object),
                        object_create_time = values(object_create_time),
                        comments = values(comments);"""

            object_ddl_start = meta_data_info_end
            object_ddl_end = len(read_file)
            object_ddl = read_file[object_ddl_start:object_ddl_end]

            object_drop_ddl_start = object_ddl.find(
                "DROP " + object_type.upper() + " IF EXISTS"
            )

            object_drop_ddl_end = object_ddl.find(";") + 1
            object_drop_ddl = object_ddl[object_drop_ddl_start:object_drop_ddl_end]

            print("Drop object DDL \n" + object_drop_ddl + "\n")
            DROP_OBJECT = object_drop_ddl
            cur.execute(object_drop_ddl)  # drop table

            print("Object Dropped " + object_type + " query : " + object_drop_ddl)

            object_create_query_start = object_drop_ddl_end + 1
            object_create_query_end = len(object_ddl)
            object_create_query = object_ddl[
                object_create_query_start:object_create_query_end
            ]

            print("Create object DDL \n" + object_create_query + "\n")
            CREATE_OBJECT = object_create_query
            cur.execute(object_create_query)  # create table

            print("INSERT meta data into table (ddl) \n" + insert_meta_data_sql + "\n")

            cur.execute(insert_meta_data_sql)

            print(
                f"\n {object_type} : {folder_object_name} created & meta data added in db- success \n"
            )  # insert meta data
            select_query_for_insert = (
                "SELECT * FROM dba.meta_data ORDER BY object_create_time DESC LIMIT 1;"
            )

            print(select_query_for_insert)
            cur.execute(select_query_for_insert)

            META_DATA = cur.fetchall()

            print("printing meta data", META_DATA)

    except Exception as e:

        print("Exception Occured", e)
