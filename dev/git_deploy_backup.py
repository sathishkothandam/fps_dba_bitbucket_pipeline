
from sshtunnel import SSHTunnelForwarder
import pymysql
with SSHTunnelForwarder(
    ('3.215.14.196'),
    ssh_username="ec2-user",
    ssh_pkey=r"fps-dev-main.pem",
    remote_bind_address=('freetier-mysql.coa8h6pjqvgc.us-east-1.rds.amazonaws.com', 3306)
) as tunnel:
    print("****SSH Tunnel Established****")
 
    db = pymysql.connect(
        host='127.0.0.1', user="admin",
        password="pw_5_20_2020", port=tunnel.local_bind_port
    )
    # Run sample query in the database to validate connection
    try:
        # Print all the databases
        with db.cursor() as cur:
            cur.execute('SHOW DATABASES')
            for r in cur:
                print(r)
    finally:
        db.close()
    print("YAYY!!")