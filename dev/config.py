class Config: 
    def __init__(
        self,
        host = 'xyz',
        user = 'xyz',
        passoword = 'xyz',
        initialDb = 'xyz',
        endpoint = 'xyz',
        username = 'xyz',
        pemLocation = 'xyz',
        port=3306,
    ):
        self._coms8_host = host
        self._coms8_user = user
        self._coms8_port = port
        self._coms8_password = passoword
        self._coms8_initial_database = initialDb
        self._ec2_endpoint = endpoint
        self._ec2_username = username
        self._ssh_pem_location = pemLocation


    def get_coms8_host(self):
        return self._coms8_host

    def set_coms8_host(self, host):
        self._coms8_host = host

    def get_coms8_user(self):
        return self._coms8_user

    def set_coms8_user(self, user):
        self._coms8_user = user

    def get_coms8_port(self):
        return self._coms8_port

    def set_coms8_port(self, port):
        self._coms8_port = port

    def get_coms8_password(self):
        return self._coms8_password

    def set_coms8_password(self, password):
        self._coms8_password = password

    def get_coms8_initial_database(self):
        return self._coms8_initial_database

    def set_coms8_initial_database(self, initailDatabase):
        self._coms8_initial_database = initailDatabase

    def get_ec2_endpoint(self):
        return self._ec2_endpoint

    def set_ec2_endpoint(self, ec2_endpoint):
        self._ec2_endpoint = ec2_endpoint

    def get_ec2_username(self):
        return self._ec2_username

    def set_ec2_username(self, ec2_username):
        self._ec2_username = ec2_username

    def get_ssh_pem_location(self):
        return self._ssh_pem_location

    def set_ssh_pem_location(self, ssh_pem_location):
        self._ssh_pem_location = ssh_pem_location
