
/*
    Create new VIEW for MySQL
    Reference 
            Naming Standards : https://fpsinc.atlassian.net/wiki/spaces/DBA/pages/836960284/SQL+Query+Standards.
            JIRA :
            Confluence : 
Steps 
    1. Add META data 
    2. CREATE OR REPLACE script.
 
************************* meta data ******************************************************************************************************
[Info] sub_type_of_object possible values are fact,dim,archive
[user edit]
client_name        = n/a
technical_owner    = sathish.kothandam@fpsinc.com
business_owner     = na
business_need      = dba
sub_type_of_object = view
decommission_date  = n/a 
comments           = test view
[/user edit]
 ************************* Create Or Replace object ****************************************************************************************
*/

DROP VIEW IF EXISTS dba.vw_test_dev ; 

CREATE VIEW dba.vw_test_dev
AS
    SELECT
        TABLE_SCHEMA                                                       AS table_schema ,
        TABLE_NAME                                                         AS table_name   ,
        TABLE_TYPE                                                         AS table_type   ,
        ENGINE                                                             AS engine       ,
        TABLE_ROWS                                                         AS table_rows   ,
        ROUND((((DATA_LENGTH + INDEX_LENGTH) / 1024) / 1024),0) AS tablemb
    FROM
        information_schema.TABLES
    WHERE 
    /* Exclude system schemas and exclude views */
        (
            (
                TABLE_SCHEMA NOT IN ('information_schema',
                                                'mysql',
                                                'performance_schema',
                                                'sys'))
        AND
            (
                TABLE_TYPE <> 'VIEW'))
                ;
