
/*

    Create new FUNCTION for MySQL
    Reference 
            Naming Standards : https://fpsinc.atlassian.net/wiki/spaces/DBA/pages/836960284/SQL+Query+Standards.
            JIRA :
            Confluence : 

Steps 

    1. Update META data about SQL object 
    2. CREATE OR REPLACE Function 
    3. Permissions 

*/

-----------------------------------------------------------------------------------------------------------------------------------------------

/*
     1. UPDATE META DATA about the object 
*/

INSERT INTO dba.db_objects_master_data
VALUES
  (
              'freetier'                                   -- instance_name
            , 'dba'                                        -- schema_name
            , 'format_seconds'                             -- object_name
            , 'in-use'                                     -- status
            , 'General'                                    -- client_name 
            , 'Sathish.Kothandam;'                         -- technical_owner
            , 'Vyomesh.Tala;'                              -- business_owner
            , 'General'                                    -- business need 
            , 'function'                                   -- type of object 
            , 'function'                                   -- sub type of object 
            , NOW()                                        -- table created datetime 
            , NOW()                                        -- table last updated datetime  
            , NULL                                         -- inception date 
            , NULL                                         -- decommission date 
            , NULL                                         -- age as per last updated date  
            , 'This function used to calculate week days between two dates' -- comments 
                )
ON DUPLICATE KEY UPDATE
            client_name = values(client_name),
            technical_owner = values(technical_owner),
            business_owner = values(business_owner),
            business_need = values(business_need),
            type_of_object = values(type_of_object),
            sub_type_of_object = values(sub_type_of_object),
            table_create_time = values(table_create_time),
            table_last_updated = values(table_last_updated),
            inception_date = values(inception_date),
            decommission_date = values(decommission_date),
            last_updated_age_in_years = values(last_updated_age_in_years),
            comments = values(comments);

/*
-----------------------------------------------------------------------------------------------------------------------------------
-- 2. CREATE OR REPLACE FUNCTION
*/

DROP FUNCTION IF EXISTS dba.format_seconds ;  

CREATE FUNCTION dba.format_seconds(sec BIGINT) RETURNS varchar(50) CHARSET latin1
BEGIN
        DECLARE res_var CHAR(50);
        SELECT
        CASE
          WHEN sec < 86400 THEN time_format(sec_to_time(sec%(3600*24)),'%H:%i:%s')
          ELSE concat(format(floor(sec/86400),0),'d ',time_format(sec_to_time(sec % 86400),'%H:%i:%s'))
        END into res_var;
	RETURN res_var;
END