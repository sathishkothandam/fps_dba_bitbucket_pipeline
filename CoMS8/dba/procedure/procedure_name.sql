
/*

    Create new PROCEDURE for MySQL
    Reference 
            Naming Standards : https://fpsinc.atlassian.net/wiki/spaces/DBA/pages/836960284/SQL+Query+Standards.
            JIRA :
            Confluence : 

Steps 

    1. Update META data about SQL object 
    2. CREATE OR REPLACE procedure
    3. Permissions 


     1. UPDATE META DATA about the object 
*/

INSERT INTO dba.db_objects_master_data
VALUES
  (
              'freetier'                                   -- instance_name
            , 'dba'                                        -- schema_name
            , 'sp_no_of_rows_in_master_data'               -- object_name
            , 'in-use'                                     -- status
            , 'General'                                    -- client_name 
            , 'Sathish.Kothandam;'                         -- technical_owner
            , 'Vyomesh.Tala;'                              -- business_owner
            , 'General'                                    -- business need 
            , 'procedure'                                  -- type of object 
            , 'procedure'                                  -- sub type of object 
            , NOW()                                        -- table created datetime 
            , NOW()                                        -- table last updated datetime  
            , NULL                                         -- inception date 
            , NULL                                         -- decommission date 
            , NULL                                         -- age as per last updated date  
            , 'This procedure used to calculate no of rows in db_objects_master_data' -- comments 
                )
ON DUPLICATE KEY UPDATE
            client_name = values(client_name),
            technical_owner = values(technical_owner),
            business_owner = values(business_owner),
            business_need = values(business_need),
            type_of_object = values(type_of_object),
            sub_type_of_object = values(sub_type_of_object),
            table_create_time = values(table_create_time),
            table_last_updated = values(table_last_updated),
            inception_date = values(inception_date),
            decommission_date = values(decommission_date),
            last_updated_age_in_years = values(last_updated_age_in_years),
            comments = values(comments);

/*
-----------------------------------------------------------------------------------------------------------------------------------------------

    2. CREATE OR REPLACE stored procedure 

 */

DROP PROCEDURE IF EXISTS dba.sp_dba_procedure_example ;

CREATE PROCEDURE dba.sp_dba_procedure_example()
BEGIN
    SELECT 
        user
    FROM mysql.user;
END
