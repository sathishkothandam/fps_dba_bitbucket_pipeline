/*
    Create new FUNCTION for MySQL
    Reference 
            Naming Standards : https://fpsinc.atlassian.net/wiki/spaces/DBA/pages/836960284/SQL+Query+Standards.
            JIRA :
            Confluence : 
Steps 
    1. Add META data 
    2. CREATE OR REPLACE script.
 
************************* meta data ******************************************************************************************************
[Info] sub_type_of_object possible values are fact,dim,archive
[user edit]
client_name        = default
technical_owner    = sathish.kothandam@fpsinc.com, vyomesh.tala@fpsinc.com
business_owner     = vyomesh.tala@fpsinc.com
business_need      = dba
sub_type_of_object = meta-data
decommission_date  = n/a 
comments           = tables stores information about user tables like businsess and technical owner details.
[/user edit]

 ************************* Create Or Replace object ****************************************************************************************
*/


DROP TABLE IF EXISTS dba.test_dev CASCADE ; 

CREATE TABLE dba.test_dev (
    instance_name       VARCHAR(32)     NOT NULL    ,
    table_create_time   TIMESTAMP                   ,
    inception_date      DATE                        ,
    decommission_date   DATE                        ,
    last_updated_age    DECIMAL(4,2)                
    ) 
; 
