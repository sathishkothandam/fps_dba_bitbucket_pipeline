/*
    Create new TABLE for MySQL
    Reference 
            Naming Standards : https://fpsinc.atlassian.net/wiki/spaces/DBA/pages/836960284/SQL+Query+Standards.
            JIRA :
            Confluence : sathish kothandam
Steps 

    1. Update META data about SQL object 
    2. Drop and Create table
*/

/*

-----------------------------------------------------------------------------------------------------------------------------------------------

     1. UPDATE META DATA about the object 
     <<Library of object ownership>>


*/

INSERT INTO dba.meta_data
VALUES
  (
   'CoMS8'                                      -- instance_name
 , 'dba'                                        -- schema_name
 , 'user_data'                                  -- object_name
 , 'in-use'                                     -- status
 , 'general'                                    -- client_name 
 , 'sathish.kothandam;'                         -- email trail of technical_owner(s)
 , 'vyomesh.tala;'                              -- email trail of business_owner(s)
 , 'general'                                    -- business need 
 , 'table'                                      -- type of object 
 , 'fact_table'                                 -- sub type of object 
 , NOW()                                        -- table created datetime 
 , NOW()                                        -- table last updated datetime  
 , NULL                                         -- inception date 
 , NULL                                         -- decommission date 
 , NULL                                         -- age as per last updated date  
 , 'test table auto dp deployement test process by sathishkothandam
    '                                           -- comments 
    )
ON DUPLICATE KEY UPDATE
  client_name = values(client_name),
  technical_owner = values(technical_owner),
  business_owner = values(business_owner),
  business_need = values(business_need),
  object_type = values(object_type),
  sub_type_of_object = values(sub_type_of_object),
  table_create_time = values(table_create_time),
  table_last_updated = values(table_last_updated),
  inception_date = values(inception_date),
  decommission_date = values(decommission_date),
  last_updated_age_in_years = values(last_updated_age_in_years),
  comments = values(comments);

/*
-----------------------------------------------------------------------------------------------------------------------------------------------

    2. Drop and Create table 
    << Developer Section >>
*/

DROP TABLE IF EXISTS dba.user_data CASCADE ; 

CREATE TABLE dba.user_data (
    instance_name       VARCHAR(32)     NOT NULL    ,
    table_create_time   TIMESTAMP                   ,
    inception_date      DATE                        ,
    decommission_date   DATE                        ,
    last_updated_age    DECIMAL(4,2)                
    ) 
; 