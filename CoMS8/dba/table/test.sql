/*
    Create new VIEW for MySQL
    Reference 
            Naming Standards : https://fpsinc.atlassian.net/wiki/spaces/DBA/pages/836960284/SQL+Query+Standards.
            JIRA :
            Confluence : 
Steps 
    1. Add META data 
    2. CREATE OR REPLACE script.
************************* meta data ******************************************************************************************************
[Info] sub_type_of_object possible values are fact,dim,archive
[user edit]
client_name        = n/a
technical_owner    = sathish.kothandam@fpsinc.com
business_owner     = vyomesh.tala@fpsinc.com
business_need      = dba
sub_type_of_object = test
decommission_date  = n/a 
comments           = test table 
[/user edit]
 ************************* Create Or Replace object ****************************************************************************************
*/
DROP TABLE IF EXISTS dba.test CASCADE ; 

CREATE TABLE dba.test
(
   instance_name              VARCHAR(24)    NOT NULL,
   schema_name                VARCHAR(64)    NOT NULL,
   object_name                VARCHAR(64)    NOT NULL,
   status                     VARCHAR(24)    NOT NULL,
   client_name                VARCHAR(64)    NOT NULL,
   technical_owner            VARCHAR(64)    NOT NULL,
   business_owner             VARCHAR(64)    NOT NULL,
   business_need              VARCHAR(64)    NOT NULL,
   object_type                VARCHAR(64)    NOT NULL,
   sub_type_of_object         VARCHAR(64)    NOT NULL,
   object_create_time         DATETIME               ,
   comments                   VARCHAR(540)   NOT NULL,
   PRIMARY KEY (instance_name, schema_name, object_name, object_type)
)
ENGINE=InnoDB;

