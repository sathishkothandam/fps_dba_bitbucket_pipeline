# This python script generates new schema folders along with object type folders in CoMS8 directory.

from sshtunnel import SSHTunnelForwarder
import pymysql
import os
import configparser

config = configparser.ConfigParser()
config.read('/config/mysql/coms8.ini')

coms8_host = config.get('master','host') 
coms8_user = config.get('master','user') 
coms8_port = 3306
coms8_password = config.get('master','password') 
coms8_initial_database = config.get('master','initial_database') 

ec2_endpoint = config.get('ec2','ec2_endpoint') 
ec2_username = config.get('ec2','ec2_username') 
ssh_pem_location = config.get('ec2','ssh_pem_location') 

host  = coms8_host 
password = coms8_password 

with SSHTunnelForwarder(
    (ec2_endpoint),
    ssh_username=ec2_username,
    ssh_pkey=ssh_pem_location,
    remote_bind_address=(host, coms8_port)
) as tunnel:

    print("***SSH Tunnel Established***")
    db = pymysql.connect(
        host='127.0.0.1', user="admin",
        password = password, port=tunnel.local_bind_port
    )
    try:
        with db.cursor(pymysql.cursors.DictCursor) as cur:

            cur.execute("show databases;")
            META_DATA = cur.fetchall()
            ignore_db = ['information_schema','innodb','mysql','performance_schema','tmp','sys'] 

            for data in META_DATA:
                if not data['Database'] in ignore_db:    
                    if not os.path.isdir('CoMS8/'+data['Database']):
                        os.mkdir('CoMS8/'+data['Database'])
                        print("new schema : " + data['Database'])

                        if not os.path.isdir('CoMS8/'+data['Database']+'/table'):
                            os.mkdir('CoMS8/'+data['Database']+'/table')
                            with open('CoMS8/'+data['Database']+'/table/table_name.sql', 'w') as fp: 
                                fp.write("--table_name.sql") 
                                pass

                        if not os.path.isdir('CoMS8/'+data['Database']+'/view'):
                            os.mkdir('CoMS8/'+data['Database']+'/view')
                            with open('CoMS8/'+data['Database']+'/view/vw_view_name.sql', 'w') as fp: 
                                fp.write("--vw_view_name.sql") 
                                pass

                        if not os.path.isdir('CoMS8/'+data['Database']+'/function'):
                            os.mkdir('CoMS8/'+data['Database']+'/function')
                            with open('CoMS8/'+data['Database']+'/function/fn_function_name.sql', 'w') as fp: 
                                fp.write("--fn_function_name.sql") 
                                pass
                            
                        if not os.path.isdir('CoMS8/'+data['Database']+'/procedure'):
                            os.mkdir('CoMS8/'+data['Database']+'/procedure')
                            with open('CoMS8/'+data['Database']+'/procedure/sp_procedure_name.sql', 'w') as fp: 
                                fp.write("--sp_procedure_name.sql") 
                                pass    
    finally:
        db.close()
        print("successfully completed")