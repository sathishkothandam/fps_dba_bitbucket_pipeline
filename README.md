
# Developer Guide

This developer guide created for users who wants to _CREATE/ALTER_ objects in production database ( **CoMS8** ).

---


[**Dev branch**](https://bitbucket.org/engineering-master/fps_dba_bitbucket_pipeline/src/dev/CoMS8/) – **Branches for all developers to test SQL**

[**Master branch**](https://bitbucket.org/engineering-master/fps_dba_bitbucket_pipeline/src/master/templates/create_table.sql) **– Branch for production SQL exists**

[**Templates**](https://bitbucket.org/engineering-master/fps_dba_bitbucket_pipeline/src/master/templates/) **– Bitbucket folder where SQL templates stored.**

[**Pipelines**](https://bitbucket.org/engineering-master/fps_dba_bitbucket_pipeline/addon/pipelines/deployments) **– Bitbucket pipeline page to monitor dev &amp; master deployment process.**

[**Confluence**](https://fpsinc.atlassian.net/wiki/spaces/DBA/pages/997621792/DB+Deployment+Automation) **– Confluence page about entire automation and platform**

---

Step by step work instructions for each activity.

## 1. Add table

1. Go to bitbucket " **dev**" branch and choose appropriate schema folder. _for example, I want to create table under_ _ **dba** _ _schema. so, I go to "dba" folder and add file with table name inside " __**table**__"._
2. File name should be table name, for ex. If table name is "user\_data" and file name should be "user\_data.sql"
3. Copy file content from template to new file. For ex. Copy entire content from table template ([Link](https://bitbucket.org/engineering-master/fps_dba_bitbucket_pipeline/src/master/templates/create_table.sql)) to new file "user\_data".sql
4. Content which you copied from template has two parts
    - Meta data - Update meta data in INSERT statement, you need to update following details _(instance\_name, schema\_name, object\_name, status, client\_name, technical\_owner, business\_owner, business\_need, object\_type, sub\_type\_of\_object, comments)_
    - DDL - Remove existing template DDL and add actual table DDL.
5. At the end file should look like below **(as in page2)**
6. Commit the file to test code at dev pipeline.

---

## 2. Add view
Steps will same as in Add table (instead of **table** choose **view** folder)

---

## 3. Add function
Steps will same as in Add table (instead of **table** choose **function** folder)

---

## 4. Add procedure
Steps will same as in Add table (instead of **table** choose **procedure** folder)

---

**What is pull request?**

When your sql code successfully tested in development and now it's ready for production, you can create a pull request to master(production) so that given sql will execute in production when approved.

**Title:** _CoMS8 Coder Review object\_name.sql_

**Description:** _object\_name.sql successfully tested in dev Pipeline-Link_

**Reviewers:** Add any admins users to review and approve the final code.

_Pull request page looks like this_

---


**What is bitbucket Pipelines?**

**Bitbucket Pipelines**  is an integrated CI/CD service built into  **Bitbucket**. It allows you to automatically build, test, and even deploy your code based on a configuration file in your repository.We've two pipelines for dev and master branches and pipelines will be triggered whenever new files committed in branch.

---


**Notifications:**

Post execution in production all users (developers, admin &amp; business users) will be notified through email (Amazon SES). Please verify/confirm email Amazon SES (simple email service). You need to verify 1st time in order to receive alerts.


---

# Markdown Cheat Sheet

This Markdown cheat sheet provides a quick overview of all the Markdown syntax elements. It can’t cover every edge case, so if you need more information about any of these elements, refer to the reference guides for [basic syntax](https://www.markdownguide.org/basic-syntax) and [extended syntax](https://www.markdownguide.org/extended-syntax).

## Basic Syntax

These are the elements outlined in John Gruber’s original design document. All Markdown applications support these elements.

### Heading

# H1
## H2
### H3

### Bold

**bold text**

### Italic

*italicized text*

### Blockquote

> blockquote

### Ordered List

1. First item
2. Second item
3. Third item

### Unordered List

- First item
- Second item
- Third item

### Code

`code`

### Horizontal Rule

---

### Link

[title](https://www.example.com)

### Image

![alt text](image.jpg)

## Extended Syntax

These elements extend the basic syntax by adding additional features. Not all Markdown applications support these elements.

### Table

| Syntax | Description |
| ----------- | ----------- |
| Header | Title |
| Paragraph | Text |

### Fenced Code Block

```
{
  "firstName": "John",
  "lastName": "Smith",
  "age": 25
}
```

### Footnote

Here's a sentence with a footnote. [^1]

[^1]: This is the footnote.

### Heading ID

### My Great Heading {#custom-id}

### Definition List

term
: definition

### Strikethrough

~~The world is flat.~~

### Task List

- [x] Write the press release
- [ ] Update the website
- [ ] Contact the media
