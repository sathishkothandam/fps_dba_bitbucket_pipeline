/*
    Create new FUNCTION for MySQL
    Reference 
            Naming Standards : https://fpsinc.atlassian.net/wiki/spaces/DBA/pages/836960284/SQL+Query+Standards.
            JIRA :
            Confluence : 
Steps 
    1. Add META data 
    2. CREATE OR REPLACE script.
 
************************* meta data ******************************************************************************************************
[Info] sub_type_of_object possible values are fact,dim,archive
[user edit]
client_name        = default
technical_owner    = sathish.kothandam@fpsinc.com, vyomesh.tala@fpsinc.com
business_owner     = vyomesh.tala@fpsinc.com
business_need      = dba
sub_type_of_object = procedure
decommission_date  = n/a 
comments           = function calculates no of rows in db_objects_master_data table.
[/user edit]

 ************************* Create Or Replace object ****************************************************************************************
*/

--/
CREATE PROCEDURE 
    dba.sp_count_of_user_objects()
    READS SQL DATA
BEGIN
    SELECT
        COUNT(*) ctr
    FROM
        (   SELECT 
                * 
            FROM 
                dba.db_objects_master_data )sa ;
 
END
/
;