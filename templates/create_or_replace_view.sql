/*
    Create new VIEW for MySQL
    Reference 
            Naming Standards : https://fpsinc.atlassian.net/wiki/spaces/DBA/pages/836960284/SQL+Query+Standards.
            JIRA :
            Confluence : 
Steps 
    1. Add META data 
    2. CREATE OR REPLACE script.
 
************************* meta data ******************************************************************************************************
[Info] sub_type_of_object possible values are fact,dim,archive
[user edit]
client_name        = default
technical_owner    = sathish.kothandam@fpsinc.com, vyomesh.tala@fpsinc.com
business_owner     = vyomesh.tala@fpsinc.com
business_need      = dba
sub_type_of_object = view
decommission_date  = n/a 
comments           = view calculates total week days bw two dates.
[/user edit]

 ************************* Create Or Replace object ****************************************************************************************
*/

DROP VIEW IF EXISTS dba.vw_user_tables ; 

CREATE VIEW dba.vw_user_tables
AS
    SELECT
        `tables`.`TABLE_SCHEMA`                                                       AS `table_schema` ,
        `tables`.`TABLE_NAME`                                                         AS `table_name`   ,
        `tables`.`TABLE_TYPE`                                                         AS `table_type`   ,
        `tables`.`ENGINE`                                                             AS `engine`       ,
        `tables`.`TABLE_ROWS`                                                         AS `table_rows`   ,
        ROUND((((`tables`.`DATA_LENGTH` + `tables`.`INDEX_LENGTH`) / 1024) / 1024),0) AS `tablemb`
    FROM
        `information_schema`.`TABLES` `tables`
    WHERE 
    /* Exclude system schemas and exclude views */
        (
            (
                `tables`.`TABLE_SCHEMA` NOT IN ('information_schema',
                                                'mysql',
                                                'performance_schema',
                                                'sys'))
        AND
            (
                `tables`.`TABLE_TYPE` <> 'VIEW'))
                ;