/*

There are two options to add(Or remove) column from a table 

1. USING create_table.sql template
    Through this template we can recreate ( delete and create) table, means when we add new column data in table will be deleted and recreated 
2. Using ALTER
    create a file using below format
    table_name_ALTER_YYYYMMDD.sql


ALTER Syntax 
-------------

ALTER TABLE table
ADD [COLUMN] column_name_1 column_1_definition [FIRST|AFTER existing_column],
ADD [COLUMN] column_name_2 column_2_definition [FIRST|AFTER existing_column],
...;


*/

ALTER TABLE vendors
ADD COLUMN phone VARCHAR(15) AFTER name;
