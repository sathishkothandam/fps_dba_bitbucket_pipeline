/*
    Create new FUNCTION for MySQL
    Reference 
            Naming Standards : https://fpsinc.atlassian.net/wiki/spaces/DBA/pages/836960284/SQL+Query+Standards.
            JIRA :
            Confluence : 
Steps 
    1. Add META data 
    2. CREATE OR REPLACE script.
 
************************* meta data ******************************************************************************************************
[Info] sub_type_of_object possible values are fact,dim,archive
[user edit]
client_name        = default
technical_owner    = sathish.kothandam@fpsinc.com, vyomesh.tala@fpsinc.com
business_owner     = vyomesh.tala@fpsinc.com
business_need      = dba
sub_type_of_object = function
decommission_date  = n/a 
comments           = function calculates total week days bw two dates.
[/user edit]

 ************************* Create Or Replace object ****************************************************************************************
*/

--/
DROP FUNCTION IF EXISTS dba.total_weekdays ;  
CREATE FUNCTION dba.total_weekdays(date1 DATE, date2 DATE) RETURNS INT(11)
RETURN ABS(DATEDIFF(date2, date1)) + 1
     - ABS(DATEDIFF(ADDDATE(date2, INTERVAL 1 - DAYOFWEEK(date2) DAY),
                    ADDDATE(date1, INTERVAL 1 - DAYOFWEEK(date1) DAY))) / 7 * 2
     - (DAYOFWEEK(IF(date1 < date2, date1, date2)) = 1)
     - (DAYOFWEEK(IF(date1 > date2, date1, date2)) = 7)
/
